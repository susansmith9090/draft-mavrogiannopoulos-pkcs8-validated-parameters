<?xml version="1.0"?>
<!DOCTYPE rfc SYSTEM "rfc2629.dtd" [
<!ENTITY RFC5208 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.5208.xml">
<!ENTITY RFC5958 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.5958.xml">
<!ENTITY RFC2119 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.2119.xml">
<!ENTITY RFC5912 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.5912.xml">
<!ENTITY RFC8017 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.8017.xml">
<!ENTITY RFC8174 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.8174.xml">
]>
<?rfc toc="no"?>
<?rfc symrefs="yes"?>
<rfc ipr="trust200902" category="info" docName="draft-mavrogiannopoulos-pkcs8-validated-parameters-05">
  <front>
    <title abbrev="Storing validation parameters in PKCS#8">Storing validation parameters in PKCS#8</title>
    <author initials="N." surname="Mavrogiannopoulos" fullname="Nikos Mavrogiannopoulos">
      <organization abbrev="Red Hat">Red Hat, Inc.</organization>
      <address>
        <postal>
          <street/>
          <city>Brno</city>
          <region/>
          <country>Czech Republic</country>
        </postal>
        <email>nmav@redhat.com</email>
      </address>
    </author>
    <date month="Sep" year="2018"/>
    <area>Security</area>
    <keyword>I-D</keyword>
    <keyword>Internet-Draft</keyword>
    <keyword>Private keys</keyword>
    <keyword>Shawe-Taylor prime generation</keyword>
    <keyword>Private key validation parameters</keyword>
    <keyword>PKCS#8</keyword>
    <abstract>
      <t>
This memo describes a method of storing parameters needed for private key validation
in the Private-Key Information Syntax Specification as defined in RFC5208
(PKCS#8) format. It is equally applicable to the alternative implementation
of the Private-Key Information Syntax Specification as defined in RFC 5958.
         </t>
         <t>
The approach described in this document encodes the parameters under a private enterprise
extension and does not form part of a formal standard.
         </t>
    </abstract>
  </front>
  <middle>
    <section anchor="intro" title="Introduction">
<t>
RSA or DSA private keys generated using the Shawe-Taylor prime generation
algorithm describled in <xref target="FIPS186-4"/> allow for parameter validation,
i.e., verify whether the primes are actually prime, and were correctly generated.
That is done by generating the parameters from a known seed and a selected hash algorithm.
</t>
<t>
Storing these parameters in a private key format such as the RSA Private Key Syntax
from PKCS#1 <xref target="RFC8017"/>, or common representations for DSA private keys,
does not allow attaching information on the parameters needed for validation. The
purpose of the document is to describe such a method using the Private-Key Information
Syntax Specification as defined in <xref target="RFC5208"/>, as well as on the alternative
specification on <xref target="RFC5958"/>.
</t>
<t>
The approach described in this document encodes the parameters under a private enterprise
extension and does not form part of a formal standard. The encoding can be used as is,
or could be used as the basis for a standard at a later time.
</t>
    </section>
    
        <section anchor="pkcs8" title="ValidationParams attribute">
<t>
The information related to the validation parameters is stored as
an attribute in the PrivateKeyInfo structure. The attribute is
identified by the id-attr-validation-parameters object identifier
and contains as AttributeValue a single ValidationParams structure.
</t>
<t>
<figure>
<artwork><![CDATA[
  id-attr-validation-parameters OBJECT IDENTIFIER ::=
                                           {1 3 6 1 4 1 2312 18 8 1}

  ValidationParams ::= SEQUENCE {
      hashAlgo OBJECT IDENTIFIER,
      seed OCTET STRING
  }
]]></artwork>
</figure>
</t>
<t>
The algorithm identifier in the ValidationParams should be a hash algorithm
identifier for the <xref target="FIPS186-4"/> methods. The ValidationParams
sequence must be DER encoded <xref target="CCITT.X690.2015"/>.
</t>
</section>

        <section anchor="example" title="Example Structure">
<t>
The following structure contains an RSA key generated using
the <xref target="FIPS186-4"/> section B.3.3 algorithm with
SHA2-384 hash. The seed used is
'8af4328c87bebcec31e303b8f5537effcb6a91d947084d99a369823b36f01462'
(hex encoded).
</t>
<t>
<figure>
<artwork><![CDATA[
-----BEGIN PRIVATE KEY-----
MIIE/gIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCpPwXwfhDsWA3q
jN2BWg1xfDjvZDVNfgTV/b95g304Aty3z13xPXAhHZ3ROW3pgPxTj9fiq7ZMy4Ua
gMpPK81v3pHX1uokC2KcGXbgbAq2Q8ClxSXgEJllRwDENufjEdV10gArt8NlIP0N
lota1kQUuI1DMsqc5DTIa35Nq4j1GW+KmLtP0kCrGq9fMGwjDbPEpSp9DTquEMHJ
o7kyJIjB+93ikLvBUTgbxr+jcnTLXuhA8rC8r+KXre4NPPNPRyefRcALLt/URvfA
rTvFOQfi3vIjNhBZL5FdC+FVAr5QnF3r2+cuDPbnczr4/rr81kzFGWrwyAgF5FWu
pFtB5IYDAgMBAAECggEAHZ88vGNsNdmRkfhWupGW4cKCuo+Y7re8Q/H2Jd/4Nin2
FKvUPuloaztiSGDbVm+vejama/Nu5FEIumNJRYMeoVJcx2DDuUxO1ZB1aIEwfMct
/DWd0/JDzuCXB0Cu5GTWLhlz0zMGHXihIdQ0DtGKt++3Ncg5gy1D+cIqqJB515/z
jYdZmb0Wqmz7H3DisuxvnhiCAOuNrjcDau80hpMA9TQlb+XKNGHIBgKpJe6lnB0P
MsS/AjDiDoEpP9GG9mv9+96rAga4Nos6avYlwWwbC6d+hHIWvWEWsmrDfcJlm2gN
tjvG8omj00t5dAt7qGhfOoNDGr5tvJVo/g96O/0I8QKBgQDdzytVRulo9aKVdAYW
/Nj04thtnRaqsTyFH+7ibEVwNIUuld/Bp6NnuGrY+K1siX8+zA9f8mKxuXXV9KK4
O89Ypw9js2BxM7VYO9Gmp6e1RY3Rrd8w7pG7/KqoPWXkuixTay9eybrJMWu3TT36
q7NheNmBHqcFmSQQuUwEmvp3MQKBgQDDVaisMJkc/sIyQh3XrlfzmMLK+GlPDucD
w5e50fHl8Q5PmTcP20zVLhTevffCqeItSyeAno94Xdzc9vZ/rt69410kJEHyBO9L
CmhtYz94wvSdRhbqf4VzAl2WU184sIYiIZDGsnGScgIYvo6v6mITjRhc8AMdYoPR
rL6xp6frcwKBgFi1+avCj6mFzD+fxqu89nyCmXLFiAI+nmjTy7PM/7yPlNB76qDG
Dil2bW1Xj+y/1R9ld6S1CVnxRbqLe+TZLuVS82m5nRHJT3b5fbD8jquGJOE+e+xT
DgA0XoCpBa6D8yRt0uVDIyxCUsVd5DL0JusN7VehzcUEaZMyuL+CyDeRAoGBAImB
qH6mq3Kc6Komnwlw4ttJ436sxr1vuTKOIyYdZBNB0Zg5PGi+MWU0zl5LDroLi3vl
FwbVGBxcvxkSBU63FHhKMQw7Ne0gii+iQQcYQdtKKpb4ezNS1+exd55WTIcExTgL
tvYZMhgsh8tRgfLWpXor7kWmdBrgeflFiOxZIL1/AoGAeBP7sdE+gzsh8jqFnVRj
7nOg+YllJAlWsf7cTH4pLIy2Eo9D+cNjhL9LK6RaAd7PSZ1adm8HfaROA2cfCm84
RI4c7Ue0G+N6LZiFvC0Bfi5SaPVAExXOty8UqjOCoZavSaXBPuNcTXZuzswcgbxI
G5/kaJNHoEcdlVsPsYWKRNKgPzA9BgorBgEEAZIIEggBMS8wLQYJYIZIAWUDBAIC
BCCK9DKMh7687DHjA7j1U37/y2qR2UcITZmjaYI7NvAUYg==
-----END PRIVATE KEY-----
]]></artwork>
</figure>
</t>
</section>

<section anchor="notes" title="Compatibility notes">
<t>
For compatibility it is recommended that implementations following this
document, support generation and validation using the SHA2-384 hash algorithm.
</t>
<t>
The extension defined in this document is applicable both to the Private-Key
Information Syntax Specification (PKCS#8) defined <xref target="RFC5208"/> and
to Asymmetric Key Packages <xref target="RFC5958"/>.
</t>
</section>

    <section anchor="security" title="Security Considerations">
<t>
All the considerations in <xref target="RFC5208"/> and <xref target="RFC5958"/> apply.
</t>
</section>
    <section anchor="IANA" title="IANA Considerations">
      <t>
              None.
          </t>
    </section>
  </middle>
  <back>
    <references title="Normative References">

	<!--?rfc include="http://xml.resource.org/public/rfc/bibxml/reference.RFC.5208.xml"?-->
	&RFC5208;

	<?rfc include="https://xml2rfc.tools.ietf.org/public/rfc/_bibxml2/_reference.CCITT.X680.2015.xml"?>
	<?rfc include="https://xml2rfc.tools.ietf.org/public/rfc/_bibxml2/_reference.CCITT.X690.2015.xml"?>

	<reference anchor='FIPS186-4'>
	<front>
	    <title>FIPS PUB 186-4: Digital Signature Standard (DSS)</title>
	    <author 
	    initials='C. F.' surname='Kerry'
		    fullname='Cameron F. Kerry'>
	    </author>
	    <author initials='P. D.' surname='Gallagher'
		    fullname='Patrick D. Gallagher'>
	    </author>

	    <date month='July' year='2013' />
	</front>
	<seriesInfo name="FEDERAL INFORMATION PROCESSING STANDARDS PUBLICATION" value=""/>
	</reference>

	&RFC5958;

   	  </references>
    <references title="Informative References">
	<!--?rfc include="http://xml.resource.org/public/rfc/bibxml/reference.RFC.8017.xml"?-->
	&RFC8017;

	&RFC5912;



   	  </references>
      <section title="Acknowledgements">
      <t>
The author would like to thank Russ Housley for his comments and for the
ASN.1 module appendix.
      </t>
      </section>
      <section anchor="asn1" title="ASN.1 module">
      <t>
This appendix provides non-normative ASN.1 definitions for the
structures described in this specification using ASN.1 as defined in
<xref target="CCITT.X680.2015"/> and <xref target="RFC5912"/>.
      </t>
      <t>
      <figure>
<artwork><![CDATA[
   PrivateKeyValidationAttrV1
     { iso(1) identified-organization(3) dod(6) internet(1)
       private(4) enterprise(1) 2312 18 1 1 }

   DEFINITIONS IMPLICIT TAGS ::=

   BEGIN

   -- EXPORTS ALL

   IMPORTS

   ATTRIBUTE
    FROM PKIX-CommonTypes-2009  --  [RFC5912]
      { iso(1) identified-organization(3) dod(6) internet(1)
        security(5) mechanisms(5) pkix(7) id-mod(0)
        id-mod-pkixCommon-02(57) } ;

   -- PrivateKeyInfo is defined in [RFC5208].
   -- This definition adds the validation parameters attribute
   -- to the set of allowed attributes.

   PrivateKeyInfo ATTRIBUTE ::= {
     at-validation-parameters, ... }

   at-validation-parameters ATTRIBUTE ::= {
     TYPE ValidationParams
     IDENTIFIED BY id-attr-validation-parameters }

   id-attr-validation-parameters OBJECT IDENTIFIER ::=
     { 1 3 6 1 4 1 2312 18 8 1 }

   ValidationParams ::= SEQUENCE {
     hashAlg OBJECT IDENTIFIER,
     seed OCTET STRING }

   END
]]></artwork>
</figure>

      </t>
      
      </section>
  </back>
</rfc>
